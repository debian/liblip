**************************************************************************
*                                                                        *
*     CLASS LIBRARY LIP FOR MULTIVARIATE SCATTERD DATA INTERPOLATION     *
*                                                                        *
*                   GNU GENERAL PUBLIC LICENSE                           *
*                      Version 2, June 1991                              *
*                                                                        *
*    Everyone is permitted to copy and distribute verbatim copies        *
*    of this license document, but changing it is not allowed.           *
*                                                                        *
*    Report Bugs to <gleb@deakin.edu.au, eteban@v7w.com>                 *
*                                                                        *
**************************************************************************

README FILE
============

Table of Contents:
------------------

	1. Tarball Contents
	2. LibLip Installation Guide
		2.1 Using lipinstall Script
		2.2 Installing as Superuser (root access)
		2.3 Installing Without Root Access
	3. Info Manual
	4. linking to the library
		4.1 linking to library in search path
		4.2 linking to library not in search path
		4.3 compiling examples
	5. Uninstall
	6. Trouble Shooting

-------------Important notes about linking: see sec. 4 ---------------

====================
1. Tar ball Contents
====================

Once you have extracted the source files and various installation scripts
form the tar ball "liblip2.0.0.tar.gz" you should find therein the following:

	- lipinstall:		Script that helps to install the library.

	- lipuninstall:		Script that helps to uninstall the library.

	- src directory:	Contains source and header files listed bellow.

	- Source Code :		consist of the following:
					* interpol.cpp
					* liblip.cpp
					* forest.cpp
					* slipint.cpp

	- Header Files:		consist of the following:
					* liblip.h
					* liblipc.h
					* forest.h
					* slipint.h

	- INCLUDE			additional headers from glpk and tnt libraries

	- EXAMPLES:		Examples directory includes example code and
				a makefile which illustrates how to link to
				shared library and static library.

	- DOCS:		Docs directory includes the description of the library and
				other documentation which might be of interest to the user.


============================
2. LibLip Installation Guide
============================

Installation of LibLip can be done in two ways: as a normal user, or ideally as a
superuser. Both ways are very similar and are described bellow. INSTALLATION MUST BE
CARRIED OUT FROM THE DIRECTORY INTO WHICH THE TARBALL WAS EXTRACTED.

---------------------------
2.1 Using lipinstall script
---------------------------

Included with this distribution is the "lipinstall' script that runs "configure", "make
install" and installs include documentation as described in sections 2.1 and 2.2.
lipinstall script must be used in the following manner.

	lipinstall [ | installation_directory ]

Example: lipinstall ~/liblip2
	
If no arguments are passed to the script then it tries to install the liblip library in 
/usr/local (as described in 2.2).
If an installation_directory is given then the script will try and do the install in 
the given directory ( see section 2.3 ).

------------------------------------------
2.2 Installing as Superuser (root access)
------------------------------------------

If you do not have an account with root access go to section 2.2. Otherwise
follow the instructions bellow.

1. login as root in any shell, using the command shown below

	e.g. shell-promp$ su -

2. run lipinstall script. It will install the library and other documentation.

	e.g. shell-promp$ ./lipinstall

the above example installs the library in the default directory "/usr/local/".
The library and other files will be installed as follows:

	- library files  		"/usr/local/lib/"
	- *.h files			"/usr/local/include"
	- examples			"/usr/local/share/doc/lip.2.0/DOCS"
	- documents			"/usr/local/share/doc/lip.2.0/EXAMPLES"

3. at this time the library should be installed in /usr/local/lib (as
suggested by the GNU standards for libraries that are not part of the
system).

Most Linux distributions have included the /usr/local/lib path in the
/etc/ld.so.conf file, which stores all search paths the OS searches when
loading shared libraries at boot time. red hat does not do this so add it.

4. once step 3 is completed, in order not to have to restart the computer
just run /sbin/ldconfig which loads shared libraries form the path stored
in /etc/ld.so.conf. You will not have to do this again, as it gets done
automatically every time the OS boots.

5. You are ready to use Lip. Please read linking instructions in section 4.1

-----------------------------------
2.3 Installing Without Root Access
-----------------------------------

If your account does not allow for root access, then install Lip into your home 
directory following these instructions.

1. run lipinstall script along with the directory you want Lip to be installed to. 

	e.g. shell-promp$ ./lipinstall /home/user_name/liblip2

the above example installs the library in the directory "/home/user_name/liblip2".
When you install the library replace user_name with your actual user name, or enter a 
different directory.

The library and other files will be included as follows:

	- library files  		"/home/user_name/liblip2/lib/"
	- *.h files			"/home/user_name/liblip2/include"
	- examples			"/home/user_name/liblip2/DOCS"
	- documents			"/home/user_name/liblip2/EXAMPLES"

2. to link and compile please read section 4.2

===============
3.  Manual
===============

The manual is liblip2.pdf file, in the DOCS directory. It describes how to use the library. 

==========================
4. Linking to the library
==========================

--------------------------------------------------------
4.1 linking to the library in search path (shared library)
--------------------------------------------------------

If you have installed the library in the default path /usr/local/lib and this
directory exists in /etc/ld.so.conf and loaded using /sbin/ldconfig ,then
linking and compiling your code is simple.

	1. make sure that the appropriate headers are placed in your code
	------------------------------------------------------------
	#include <liblip.h>
	#include <liblipc.h>
	------------------------------------------------------------

	2. Assume your code is in file named example.cpp. Then to compile and link use

	------------------------------------------------------------
	g++ -c example.cpp
	g++ -o example example.o -llip -lglpk -lm

	------------------------------------------------------------

the resulting executable will be "example".

-----------------------------------------
4.2 linking to library not in search path
-----------------------------------------

If you have installed the library in a directory that is not listed in
/etc/ld.so.conf, then there are a few options:

	1. If you have installed LibLip to your home directory then use static linking.
	Example:

	a. make sure you have included the right headers, for example
	------------------------------------------------------------
	#include "../include/liblip.h"
	#include "../include/liblipc.h"
	------------------------------------------------------------

	b. Use -I option to tell the compiler where to locate header files
	and then link to liblip.a (static library) directly.
	------------------------------------------------------------

	g++ -c example.cpp -I/home/user_name/liblip2/include

	g++ -o example -non_shared example.o /home/user_name/Liblip2/lib/liblip.a -lm
	// instead of -non_shared, some compilers require -static option, or none at all 
	
	------------------------------------------------------------

	Note that liblip uses glpk library (version 4.8 or 4.9). Locate glpk header and
	library files and provide the path to these files in you makefile

      Alternatively: 
	2. Get the system administrator to add the installation directory to
	/ect/ld.so.conf file. This could be /home/user_name/liblip2/lib.

	3. Add the library directory to the environment variable LD_LIBRAY_PATH,
	used during linking.

	4. Finally you can use the `-Wl,--rpath -Wl,LIBDIR' linker flags. Note
	that if you do not use -Wl option then the compiler may quietly drop
	away linker flags.

-----------------------------------------
4.3 compiling the examples
-----------------------------------------

	1. change to EXAMPLES directory

	2. Edit makefile if necessary, and specify the path to the directories you
	installed LibLip to: change the lines specifying LIB_PATH  and INCLUDE_PATH,
	e.g. LIB_PATH = /home/user_name/liblip2/lib/
	INCLUDE_PATH = /home/user_name/liblip2/include/
	and also 

	GLPK_LIB_PATH=/home/glpklib/lib
	
	depending on where you installed glpk	

	3. edit the following header file, as it points to the location of "glpk.h"
		 slipint.h

	4. run "make" command

	If you did not install LibLip to the default directories, building of shared_example and static_example
	targets may not work. However targets static_example2 and static_example3 will compile,
	as in both case static linking is used.


===================
5. Uninstall LibLip
===================

To uninstall simply run the lipuninstall script at the promp.

	e.g. shell-promp$ ./lipuninstall

====================
6. Troubleshooting
====================

1. Compilation:

	- depemding on how the local C++ compiler has been installed on a given machine, there might be a
	need to give it some different options. To do so in the file "Makefile" look for the variable
	liblip_la_CXXFLAGS and assign it the right options.

	- all the source files needed to compile the library are included with this distribution, so if you are
	having trouble with the autotools you can try and compile it yourself. (e.g. make your own makefile or 
	use libtool).

